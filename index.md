---
title: Mémoire et performance
author: Xavier L'Heureux
date: 11 mars 2020
---

## Pourquoi apprendre?

 - Un ordinateur est un outil qui transforme des données. Ignorer les données dans le processus est absurde
 - Le memory wall (~1990)
 - Pas correct de gaspiller l'argent des utilisateurs en gaspillant puissance de calcul.

# Rappel sur le RAM machine

# Background électronique

## SRAM

![Static RAM](./images/sram.png)

## DRAM

![Dynamic RAM](./images/dram.png)

## SRAM

 - (+) Rapide
 - (+) Pas besoin de refresh
 - (-) Coûteux
 - (-) Gros (relativement) = faible densité

## DRAM

 - (+) Pas cher
 - (+) Petit = grande densité
 - (-) Besoin de contrôleur
 - (-) Relativement lent

## Distances physiques

 - ~30cm entre le RAM et le CPU
 - 1,5×10⁸ m/s (mettons) pour les électrons
 - 2×10⁻⁹ s pour le transfert
 - À 4 GHz, 16 cycles pour un allez-retour

# Comment en profiter?

## Caches

![Hiérarchie des caches](./images/cache.jpg)

## Tailles

- L3: 8MB à 32MB
- L2: 256KB
- L1: 32KB instruction + 32KB données

## Comparaison

<div id="vitesses">
<div class="choice">
  <span class="desc">Register (1 cycle)</span><span class="dot" id="register"></span>
</div>
<div class="choice">
  <span class="desc">L1 (4 cycles)</span><span class="dot" id="l1"></span>
</div>
<div class="choice">
  <span class="desc">L2 (10 cycles)</span><span class="dot" id="l2"></span>
</div>
<div class="choice">
  <span class="desc">L3 (75 cycles)</span><span class="dot" id="l3"></span>
</div>
<div class="choice">
  <span class="desc">RAM (130 cycles)</span><span class="dot" id="ram"></span>
</div>
</div>

# Prefetching

## Accélération des accès à la mémoire

- Accès typiquement linéaires
- Facile d'accès pour le RAM
- Peut éviter un round-trip

## Implications sur les performances

- Vecteur mieux que linked list pour l'insertion
- O(n) vs O(1) biaisé
- Le coût caché des modifications

# Instruction cache eviction

## Branching

- If au niveau des instructions
- "hot path" et "cold path"

## Dynamic dispatch

- Type de la variable inconnue à la compilation
- Flexibilité ++
- Indirection supplémentaire
- Aucune prédiction pour l'accès mémoire
- Inlining impossible

# Le cas Java
